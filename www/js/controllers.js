angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state, AuthService, $ionicPopup, $ionicLoading, $rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  $scope.$on('$ionicView.enter', function(e) {
    if(AuthService.getStatusLogin()){
      $scope.loginData.name = AuthService.name();
      $scope.loginData.photo = AuthService.photo();
    }
  });

  // Form data for the login modal
  $scope.loginData = {};

  $rootScope.$on('$stateChangeStart', function(event, $scope, toState, toParams, fromState, fromParams) {

      $rootScope.statusLogin = AuthService.getStatusLogin();
      // alert($rootScope.statusLogin);
      if ($rootScope.statusLogin == null) {
          $rootScope.statusLogin = true;
          $rootScope.statusLogout = false;
      } else {
          $rootScope.statusLogin = false;
          $rootScope.statusLogout = true;
      }
  });

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {

    $ionicLoading.show({
        template: 'loading...'
    });
    AuthService.login($scope.loginData.id, $scope.loginData.password).then(function(response) {
        $scope.all = response.data;
        console.log($scope.all );
        console.log($rootScope.statusLogin);
        $ionicLoading.hide();
        if ($scope.all.status == 'success') {
          $scope.loginData.name = AuthService.name();
          $scope.loginData.photo = $scope.all.photo;
          $state.go('app.posts');
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Please check your credentials again!'
            });
            AuthService.setStatusLogin(false);
        }
    }, function(error) {
        console.error(error);
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
            title: 'Login failed!',
            template: 'Please check your credentials again!'
        });
    });
  };


  $scope.doRegister = function() {
    $ionicLoading.show({
        template: 'loading...'
    });
    AuthService.register($scope.loginData.fname, $scope.loginData.lname, $scope.loginData.address, $scope.loginData.province, $scope.loginData.tel, $scope.loginData.email, $scope.loginData.password, $scope.loginData.usertype).then(function(response) {
        $scope.all = response.data;
        $ionicLoading.hide();
        if ($scope.all.status == 'success') {
          AuthService.login($scope.loginData.email, $scope.loginData.password).then(function(response) {
              $scope.all = response.data;
              if ($scope.all.status == 'success') {
                $scope.loginData.email = AuthService.email();
                $state.go('app.dashboard', {}, {
                    reload: true
                });
              } else {
                  AuthService.setStatusLogin(false);
              }
          }, function(error) {
              console.error(error);
          });
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Register failed!',
                template: $scope.all.status
            });
            AuthService.setStatusLogin(false);
        }
    }, function(error) {
        console.error(error);
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
            title: 'Login failed!',
            template: 'Please check your credentials again!'
        });
    });
  };
  $scope.logout = function() {
    AuthService.logout();
    // $scope.loginData.name = '';
    $state.go('app.home', {},{reload:true});
  };
})
.controller('HomeCtrl', function($scope, $state) {
  $scope.register = function(){
    console.log('register');
    $state.go('app.privacy');
  }

  $scope.LoginwithFacebook = function () {
    console.log('LoginwithFacebook');
  }

  $scope.gotoHome = function(){
    // $state.go('app.dashboard');
    console.log('home'+$scope.loginData.id);
    $state.go('app.posts');
  }


})

.controller('PostsCtrl', function($scope, $state, SocketService, $ionicLoading, Posts, moment, AuthService, newRoom, $ionicPopup, closePost, getOwnPost) {
  $scope.doRefresh = function() {
    $ionicLoading.show({
      template: 'Loading...'
    });
      Posts.getItems().then(function(response) {
          $scope.posts = response.data;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
      }, function(error) {
          console.error(error);
      });
	};

  Posts.getItems().then(function(response) {
      $scope.posts = response.data;
      // console.log(JSON.stringify($scope.posts));
  }, function(error) {
      console.error(error);
  });
  $scope.removeItem = function (index, post_id) {
    getOwnPost.getItems(post_id, AuthService.user_id()).then(function(response) {
        if(response.data.status == 'success'){
          $ionicLoading.show({
              template: 'loading...'
          })
          closePost.getItems(post_id, '', AuthService.user_id(), 'room'+post_id).then(function(response) {
              if(response.data.status == 'success'){
                $state.go('app.posts');
                $scope.posts.splice(index, 1);
              }else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Failed!',
                    template: response.data.status
                });
              }
              $ionicLoading.hide();
          }, function(error) {
              console.error(error);
          });
        }else {
          var alertPopup = $ionicPopup.alert({
              title: 'Failed!',
              template: response.data.status
          });
        }
    }, function(error) {
        console.error(error);
    });


  };
  $scope.humanize = function(timestamp){
    return moment(timestamp).fromNow();
  };
  $scope.join = function(name, post_id){
    //sanitize the nickname
    // var name=$sanitize(name)
    if(name)
    {
      var room_name = 'room'+post_id;
      AuthService.setRoom(room_name);
        var room = {
            'room_name': room_name,
    				'user': AuthService.name(),
    				'room': room_name,
            'status': '',
        };

        SocketService.emit('join:room', room);
        var messages = [];
        var user_id=[];
        newRoom.getItems(room_name, post_id, AuthService.user_id(), messages, AuthService.name()).then(function(response) {
            // console.log('newRoom : '+ JSON.stringify(response.data));
            // if(AuthService.user_id() in response.data.user_id)
            user_id = JSON.stringify(response.data.user_id);
            console.log('response.data.user_id '+user_id);

            if(response.data.total == 4){
              if (user_id.indexOf(AuthService.user_id()) === -1) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Alert',
                    template: response.data.status
                });
              }else{
                $state.go('app.chat',{post_id:post_id});
              }

            }else{
              $state.go('app.chat',{post_id:post_id});
            }


        }, function(error) {
            console.error(error);
        });
    }
  }
})

.controller('CreatePostCtrl', function($scope, $state, newPost, AuthService, $ionicHistory, $cordovaGeolocation, $ionicLoading, $compile) {
  $scope.postData = {};
  $scope.user_id = AuthService.user_id();
  $scope.name = AuthService.name();
  $scope.photo = AuthService.photo();
  $scope.newPost = function() {
    newPost.getItems($scope.user_id, $scope.postData.message).then(function(response) {
        $scope.post = response.data;
        $state.go('app.posts');
    }, function(error) {
        console.error(error);
    });

  }

  $scope.cancel = function(){
    // $state.go('app.posts');
    $ionicHistory.goBack();
  }
  $scope.myGoBack = function()
  {
    $ionicHistory.goBack();
  }
  // function initialize() {
  //     var myloc = new google.maps.LatLng(35.433820, -97.135620);
  //     var mapOptions = {
  //         zoom: 15,
  //         center: myloc,
  //         scrollwheel: false
  //     };
  //     var map = new google.maps.Map(document.getElementById('map'),mapOptions);
  //     var marker = new google.maps.Marker({
  //         position: map.getCenter(),
  //         map: map,
  //         animation: google.maps.Animation.BOUNCE
  //     });
  //     var contentString = '<div id="content" style="dir:rtl; text-align:right;">'+
  //             '<div id="siteNotice">'+
  //             '</div>'+
  //             '<h1 id="firstHeading" class="firstHeading"><h1>title here</h1>'+
  //             '<div id="bodyContent">'+
  //             '<p>description here</p>'+
  //             '</div>'+
  //             '</div>';
  //     var infowindow = new google.maps.InfoWindow({
  //         content: contentString,
  //         maxWidth: 250
  //     });
  //     google.maps.event.addListener(marker, 'click', function() {
  //         infowindow.open(map,marker);
  //     });
  // }
  // google.maps.event.addDomListener(window, 'load', initialize);

  var options = {timeout: 10000, enableHighAccuracy: true};
  $scope.$on('$ionicView.enter', function(e) {
    // $cordovaGeolocation.getCurrentPosition(options).then(function(position){
    //
    //   var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    //
    //   var mapOptions = {
    //     center: latLng,
    //     zoom: 15,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP
    //   };
    //
    //   $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
    //
    // }, function(error){
    //   console.log("Could not get location");
    // });
  });


})
.controller('ChatsCtrl', function($scope,getChatList, AuthService, SocketService, $state, $ionicLoading, getRoom) {
  var userid=AuthService.user_id();
  getChatList.getItems(userid).then(function(response) {
      $scope.chats = response.data;
      console.log(JSON.stringify(response.data));

  }, function(error) {
      console.error(error);
  });

  $scope.doRefresh = function() {
    $ionicLoading.show({
      template: 'Loading...'
    });
      getChatList.getItems(userid).then(function(response) {
          $scope.chats = response.data;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
      }, function(error) {
          console.error(error);
      });
	};


  $scope.join = function(name){
    post_id = name.replace('room', '');
    //sanitize the nickname
    // var name=$sanitize(name)
    if(name)
    {
      var room_name = 'room'+post_id;
      AuthService.setRoom(room_name);
        var room = {
            'room_name': room_name,
            'user': AuthService.name(),
            'room': room_name,
            'status': '',
        };

        SocketService.emit('join:room', room);
        getRoom.getItems(room_name).then(function(response) {
            var post_id = response.data.post_id;
            $state.go('app.chat',{post_id:post_id});
        }, function(error) {
            console.error(error);
        });
    }
  }

})
.controller('ChatCtrl', function($scope, $state, $stateParams,AuthService,SocketService,$ionicScrollDelegate,$timeout, moment, AuthService, getPost, $ionicLoading, $ionicPopup, closePost, getMessages, updateMessages, leavePost, getOwnPost) {

  var usernm=AuthService.name();
  var userid=AuthService.id();
  var useravatar=AuthService.photo();

  var tousrnm = '';
  var tousrid= '';
  var tousravatar= '';
  getPost.getItems($stateParams.post_id).then(function(response) {
      $scope._toUserDetail = response.data;
      var tousrnm = $scope._toUserDetail.name;
      var tousrid= $scope._toUserDetail.id;
      var tousravatar= $scope._toUserDetail.photo;
  }, function(error) {
      console.error(error);
  });

  $scope.toUser = {
  _id: tousrid,
  pic: tousravatar,
  username: tousrnm
  }
  // this could be on $rootScope rather than in $stateParams
  $scope.user = {
  _id: userid,
  pic: useravatar,
  username: usernm
  };


		$scope.messages = [];
    // var messageArray=JSON.parse(AuthService.getMessage());
    var messageArray = [];
    getMessages.getItems(AuthService.getRoom(), AuthService.user_id()).then(function(response) {
      if(response.data.messages){
        messageArray = JSON.parse(response.data.messages);
        // console.log(response.data.messages);
        // console.log(messageArray.length);
        if(messageArray != null){
          for (var ln = 0; ln < messageArray.length; ln++) {
            // console.log(messageArray[ln]);
            var msg = {
              'room': messageArray[ln]['room'],
              'senderId': messageArray[ln]['senderId'],
              'receiverId': messageArray[ln]['receiverId'],
              'user': messageArray[ln]['user'],
              'photo': messageArray[ln]['photo'],
              'text': messageArray[ln]['text'],
              'status': '',
              'time': messageArray[ln]['time']
            };

            $scope.messages.push(msg);
      			$ionicScrollDelegate.scrollBottom();

          }
        }
      }

    }, function(error) {
        console.error(error);
    });


		$scope.humanize = function(timestamp){
			return moment(timestamp).fromNow();
		};

		$scope.current_room = AuthService.getRoom();

		var current_user = AuthService.name();

		$scope.isNotCurrentUser = function(user){

			if(current_user != user){
				return 'not-current-user';
			}
			return 'current-user';
		};


		$scope.sendTextMessage = function(){

			var msg = {
				'room': $scope.current_room,
				'senderId': $scope.user._id,
				'receiverId': $scope.toUser._id,
				'user': current_user,
				'photo': $scope.user.pic,
				'text': $scope.message,
        'status': '',
				'time': moment()
			};

			$scope.messages.push(msg);
			$ionicScrollDelegate.scrollBottom();

			$scope.message = '';
      AuthService.setMessage(JSON.stringify($scope.messages));

			SocketService.emit('send:message', msg);
      // console.log(JSON.stringify($scope.messages));
      updateMessages.getItems(AuthService.getRoom(), AuthService.user_id(), JSON.stringify($scope.messages)).then(function(response) {
          console.log(response.data.status);
      }, function(error) {
          console.error(error);
      });
		};


		$scope.closeRoom = function(){
			var msg = {
				'user': current_user,
				'senderId': $scope.user._id,
				'receiverId': $scope.toUser._id,
				'room': $scope.current_room,
        'photo': $scope.user.pic,
        'status': '',
				'time': moment()
			};
      $scope.data = {};

      getOwnPost.getItems($stateParams.post_id, AuthService.user_id()).then(function(response) {
          if(response.data.status == 'success'){
            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
              template: '<input type="text" ng-model="data.plate">',
              title: 'Enter plate number.',
              // subTitle: 'Close Room',
              scope: $scope,
              buttons: [
                { text: 'Cancel' },
                {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    if (!$scope.data.plate) {
                      //don't allow the user to close unless he enters plate
                      e.preventDefault();
                    } else {
                      return $scope.data.plate;
                    }
                  }
                }
              ]
            });

            myPopup.then(function(res) {
              $ionicLoading.show({
                  template: 'loading...'
              })
              closePost.getItems($stateParams.post_id, res, AuthService.user_id(), $scope.current_room).then(function(response) {
                  if(response.data.status == 'success'){
                    SocketService.emit('leave:room', msg);
                    $state.go('app.posts');
                  }else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Failed!',
                        template: response.data.status
                    });
                  }
                  $ionicLoading.hide();
              }, function(error) {
                  console.error(error);
              });
            });
          }else {
            var alertPopup = $ionicPopup.alert({
                title: 'Failed!',
                template: response.data.status
            });
          }
          $ionicLoading.hide();
      }, function(error) {
          console.error(error);
      });




		};
    $scope.leaveRoom = function(){

			var msg = {
				'user': current_user,
				'senderId': $scope.user._id,
				'receiverId': $scope.toUser._id,
				'room': $scope.current_room,
        'photo': $scope.user.pic,
        'status': '',
				'time': moment()
			};

      var confirmPopup = $ionicPopup.confirm({
          title: 'Leave room',
          template: 'Confirm close this room. It will not avaiable.'
      });

      confirmPopup.then(function(res) {
          if (res) {
              // console.log('You are sure');
              $ionicLoading.show({
                  template: 'loading...'
              })
              leavePost.getItems(AuthService.user_id(), $scope.current_room).then(function(response) {
                console.log('leave'+response.data);
                  if(response.data.status == 'success'){
                    // SocketService.emit('leave:room', msg);
                    var alertPopup = $ionicPopup.alert({
                        title: 'Complete',
                        template: 'This chat room not avaliable in Chat List, and you can join again.'
                    });
                    $state.go('app.posts');
                  }else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Failed!',
                        template: response.data.status
                    });
                  }
                  $ionicLoading.hide();
              }, function(error) {
                  console.error(error);
              });
          } else {
              // console.log('You are not sure');
          }
      });


		};


		SocketService.on('message', function(msg){
			$scope.messages.push(msg);
			$ionicScrollDelegate.scrollBottom();
		});


});
