angular.module('starter.services', [])
.service('AuthService', function($q, $http) {
  var LOCAL_TOKEN_KEY = 'Token';
  var isAuthenticated = false;
  var user_id = '';
  var id = '';
  var password = '';
  var name = '';
  var phone = '';
  var photo = '';
  var authToken;
  var statusLogin = false;
  var messages;
  var room = '';

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    var user_id = window.localStorage.getItem('user_id', user_id);
    var id = window.localStorage.getItem('id', id);
    var password = window.localStorage.getItem('password', password);
    var name = window.localStorage.getItem('name', name);
    var phone = window.localStorage.getItem('phone', phone);
    var photo = window.localStorage.getItem('photo', photo);
    var statusLogin = window.localStorage.getItem('statusLogin', statusLogin);
    if (token) {
      useCredentials(token, id, password);
    }
  }
  function storeUserCredentials(token, user_id,id, password,name, phone, photo,statusLogin) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    window.localStorage.setItem('user_id', user_id);
    window.localStorage.setItem('id', id);
    window.localStorage.setItem('password', password);
    window.localStorage.setItem('name', name);
    window.localStorage.setItem('phone', phone);
    window.localStorage.setItem('photo', photo);
    window.localStorage.setItem('statusLogin', statusLogin);
    useCredentials(token, id, password);
  }

  function useCredentials(token, id, password) {
    isAuthenticated = true;
    authToken = token;
    id = id ;
    password = password;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    isAuthenticated = false;
 //   $http.defaults.headers.common['X-Auth-Token'] = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    window.localStorage.removeItem('user_id');
    window.localStorage.removeItem('id');
    window.localStorage.removeItem('password');
    window.localStorage.removeItem('name');
    window.localStorage.removeItem('phone');
    window.localStorage.removeItem('photo');
    window.localStorage.removeItem('statusLogin');
    window.localStorage.removeItem('room');
    window.localStorage.removeItem('messages');
  }

  var login = function(id, password) {
    return $http({
        method: 'POST',
        url: 'http://localhost:8888/autaxi/login.php',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        transformRequest: function(obj) {
            var str = [];
            for (var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
        },
        data: { id: id, password: password }
    }).then(function(response) {
        items = response;
        if (items.data.status == 'success') {
            storeUserCredentials(items.data.user_id+ '.autaxi', items.data.user_id, items.data.id, items.data.password, items.data.name, items.data.phone, items.data.photo, true);
        }else{
            window.localStorage.setItem('statusLogin', false);
        }

        return items;
    });
  };
    var register = function(id, password, name, phone, photo) {
      return $http({
          method: 'POST',
          url: 'http://localhost:8888/autaxi/register.php',
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
          transformRequest: function(obj) {
              var str = [];
              for (var p in obj)
                  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },
          data: { id: id, password: password , name: name, phone: phone, photo:photo}
      }).then(function(response) {
          items = response;
          if (items.data.status == 'success') {
              storeUserCredentials(items.data.user_id+ '.autaxi', items.data.user_id,items.data.id, items.data.password, items.data.name, items.data.phone, items.data.photo, true);
              // window.localStorage.setItem('statusLogin', true);
          }else{
              window.localStorage.setItem('statusLogin', false);
          }

          return items;
      });
    };

  var logout = function() {
    destroyUserCredentials();
  };

  loadUserCredentials();

  return {
    login: login,
    logout: logout,
    register: register,
    isAuthenticated: function() {return isAuthenticated;},
    user_id: function() {user_id = window.localStorage.getItem('user_id', user_id); return user_id;},
    id: function() {id = window.localStorage.getItem('id', id);return id;},
    password: function() {password = window.localStorage.getItem('password', password);return password;},
    name: function() {name = window.localStorage.getItem('name', name);return name;},
    phone: function() {phone = window.localStorage.getItem('phone', phone);return phone;},
    photo: function() {photo = window.localStorage.getItem('photo', photo);return photo;},
    setStatusLogin: function(status) {window.localStorage.setItem('statusLogin', status);},
    setPassword: function(password) {window.localStorage.setItem('password', password);},
    getStatusLogin: function() {statusLogin = window.localStorage.getItem('statusLogin', statusLogin);return statusLogin;},
    setMessage: function(messages) {window.localStorage.setItem('messages', messages);},
    getMessage: function() {messages = window.localStorage.getItem('messages', messages);return messages;},
    setRoom: function(room) {window.localStorage.setItem('room', room);},
    getRoom: function() {room = window.localStorage.getItem('room', room);return room;},
    setUsergroup: function(usergroup) {window.localStorage.setItem('usergroup', usergroup);},
    getUsergroup: function() {usergroup = window.localStorage.getItem('usergroup', usergroup);return usergroup;},
  };
})
.factory('SocketService',function(socketFactory){
	//Create socket and connect to http://chat.socket.io
 // 	var myIoSocket = io.connect('http://chat.socket.io');
 	var myIoSocket = io.connect('http://localhost:3000');

  	mySocket = socketFactory({
    	ioSocket: myIoSocket
  	});

	return mySocket;
})
.factory('Posts', function($http) {
    var items = [];
    return {
        getItems: function() {
            return $http.get("http://localhost:8888/autaxi/posts.php").then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('getPost', function($http) {
    var items = [];
    return {
        getItems: function(id) {
            return $http.get("http://localhost:8888/autaxi/getpost.php?id="+id).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('newPost', function($http) {
    var items = [];

    return {
        getItems: function(user_id, message) {
            return $http({
                method: 'POST',
                url: 'http://localhost:8888/autaxi/newpost.php',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: { user_id: user_id, message: message }
            }).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('closePost', function($http) {
    var items = [];

    return {
        getItems: function(id, plate_number, user_id, room_name) {
            return $http({
                method: 'POST',
                url: 'http://localhost:8888/autaxi/closepost.php',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: { id: id, plate_number: plate_number, user_id: user_id, room_name: room_name }
            }).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('getOwnPost', function($http) {
    var items = [];
    return {
        getItems: function(post_id, user_id) {
            return $http.get("http://localhost:8888/autaxi/getownpost.php?id="+post_id+"&user_id="+user_id).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('leavePost', function($http) {
    var items = [];
    return {
        getItems: function(user_id, room_name) {
            return $http.get("http://localhost:8888/autaxi/leavepost.php?user_id="+user_id+"&room_name="+room_name).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('newRoom', function($http) {
    var items = [];

    return {
        getItems: function(room_name, post_id, user_id, messages, chat_name) {
            return $http({
                method: 'POST',
                url: 'http://localhost:8888/autaxi/newroom.php',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: { room_name: room_name, post_id: post_id,user_id: user_id,messages: messages ,chat_name: chat_name}
            }).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})

.factory('getRoom', function($http) {
    var items = [];
    return {
        getItems: function(room_name) {
            return $http.get("http://localhost:8888/autaxi/getroom.php?room_name="+room_name).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('getMessages', function($http) {
    var items = [];

    return {
        getItems: function(room_name, user_id) {
            return $http({
                method: 'POST',
                url: 'http://localhost:8888/autaxi/getmessages.php',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: { room_name: room_name, user_id: user_id }
            }).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})
.factory('updateMessages', function($http) {
    var items = [];
    return {
        getItems: function(room_name, user_id, messages) {
            return $http({
                method: 'POST',
                url: 'http://localhost:8888/autaxi/updatemessages.php',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: { room_name: room_name, user_id: user_id , messages: messages}
            }).then(function(response) {
                items = response;
                return items;
            });
        }
    }
})
.factory('getChatList', function($http) {
    var items = [];
    return {
        getItems: function(id) {
            return $http.get("http://localhost:8888/autaxi/getchatlist.php?id="+id).then(function(response) {
                items = response;
                return items;
            });
        }
    }  
})

;
